package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1804323570948502389L;
	/**
	 * 
	 */
	
	public void destroy(){
		System.out.println("**********************************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("**********************************************************");
	}
	public void init() throws ServletException{
		System.out.println("*********************************************************");
		System.out.println(" UserServlet has been initialized");
		System.out.println("*********************************************************");
	}
//	public void doGet(HttpServletRequest req, HttpServletResponse res)throws IOException,ServletException{
////		Url Rewriting via sendRedirect Method
////			HttpSession session = req.getSession();
////			String contact= req.getParameter("contact");
////			System.getProperties().put("contact",contact);
////			String con = System.getProperty("contact");
////			session.setAttribute("contact",contact);
////			res.sendRedirect("user?contact="+con);
//	}
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException,ServletException{
		
			String firstName= req.getParameter("firstname");
			firstName = firstName.substring(0,1).toUpperCase()+firstName.substring(1).toLowerCase();
			System.getProperties().put("fName", firstName);
			String firstN = System.getProperty("fName");
		
			String lastName= req.getParameter("lastname");
			lastName = lastName.substring(0,1).toUpperCase()+lastName.substring(1).toLowerCase();
			HttpSession session = req.getSession();
			session.setAttribute("lName", lastName);
			String lastN = session.getAttribute("lName").toString();
		
			ServletContext srvContext = getServletContext();
			String email= req.getParameter("email").toLowerCase();
			srvContext.setAttribute("email", email);
		
		String contact= req.getParameter("contact");
		System.getProperties().put("contact", contact);
		String con = System.getProperty(contact);
		res.sendRedirect("details?contact="+contact);
		PrintWriter out = res.getWriter();
		out.println("<h1>Welcome to PhoneBook</h1>"+
				"<p>First Name: "+firstN+"</p>"+
				"<p>Last Name: "+lastN+"</p>"+
				"<p>Email: "+email+"</p>"+
				"<p>Contact: "+con+"</p>");
	}
}
